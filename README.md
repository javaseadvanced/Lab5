# Exercise5
Многопоточность. Реализация модели прохождения пункта контроля с помощью семафоров

#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

# Запуск c пакетами ( успешно )

{
    "tasks": [
        {
            "type": "che",
            "label": "SemaphoreExample build and run",
            "command": "javac -d /projects/Lab5/TestConcurrency/bin -sourcepath /projects/Lab5/TestConcurrency/src /projects/Lab5/TestConcurrency/src/semaphores/SemaphoreExample.java && java -classpath /projects/Lab5/TestConcurrency/bin semaphores.SemaphoreExample",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab5/TestConcurrency/src",
                "component": "maven"
            }
        },
        {
            "type": "che",
            "label": "PhilosopherSample build and run",
            "command": "javac -d /projects/Lab5/TestConcurrency/bin -sourcepath /projects/Lab5/TestConcurrency/src /projects/Lab5/TestConcurrency/src/semaphores/PhilosopherSample.java && java -classpath /projects/Lab5/TestConcurrency/bin semaphores.PhilosopherSample",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab5/TestConcurrency/src",
                "component": "maven"
            }
        }
    ]
}
